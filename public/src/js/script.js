window.showLoading = function(fn) {
    var callback = fn;
    $('.loading').fadeIn(500);
    setTimeout(function () {
        $('.loading').fadeOut(500);
        callback && callback();
    }, 1000);
};


$(window).on('load', function () {
    var $loading = $('.loading');
    $loading.fadeOut(500, function () {
        $('.page-bg').fadeIn(500);
    });
});


$(function() {
    var $canvas = $('.card-box canvas');
    var $main = $('#main');
    var canvasWidth = $main.width();

    $canvas.attr('width', canvasWidth);
    $canvas.attr('height', canvasWidth);

    $('[data-go-step]').on('click', function (e) {
        e.preventDefault();
        var stepId = $(this).data('go-step');
        $('.page').fadeOut(500, function () {
            showLoading(function () {
                $('.page-'+stepId).show(500);
            });
        });
    });
});