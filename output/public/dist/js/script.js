(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
window.showLoading = function(fn) {
    var callback = fn;
    $('.loading').fadeIn(500);
    setTimeout(function () {
        $('.loading').fadeOut(500);
        callback && callback();
    }, 1000);
};


$(window).on('load', function () {
    var $loading = $('.loading');
    $loading.fadeOut(500, function () {
        $('.page-bg').fadeIn(500);
    });
});


$(function() {
    var $canvas = $('.card-box canvas');
    var $main = $('#main');
    var canvasWidth = $main.width();

    $canvas.attr('width', canvasWidth);
    $canvas.attr('height', canvasWidth);

    $('[data-go-step]').on('click', function (e) {
        e.preventDefault();
        var stepId = $(this).data('go-step');
        $('.page').fadeOut(500, function () {
            showLoading(function () {
                $('.page-'+stepId).show(500);
            });
        });
    });
});
},{}]},{},[1])